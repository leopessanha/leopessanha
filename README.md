Hi, I’m Léo, software engineer and computer science student.

Current work:

- Development and maintenance of Java REST APIs using Hibernate and JAX-RS
- Design and maintenance of SQL Server databases
- Agile software development + Git version control

Current technologies:

- JavaEE
- JPA
- Hibernate
- SQL Server
- JAX

Past Experience:

- Scientific Research
- Teaching: Computer Science & Game Development Principles
- Scientific Data Processing & Scientific Data Visualization (with open source and proprietary softwares)

Currently studying:

- Unit Testing
- CI/CD (Jenkins)
- Cloud Computing

Contact:    
leonardopessanha74@gmail.com   
https://www.linkedin.com/in/leopessanha/   
Telegram: @leopessanha 
